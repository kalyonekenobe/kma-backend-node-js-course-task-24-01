const square = async (number) => ({ number, square: number ** 2 });

const reverse = async (text) => text.split("").reverse().join("");

const date = async (year, month, day) => {
  const specifiedDate = new Date(`${year}-${month}-${day}`);
  const currentDate = new Date(new Date().setHours(0, 0, 0, 0));

  return {
    weekDay: new Intl.DateTimeFormat("en-US", { weekday: "long" }).format(
      specifiedDate
    ),
    isLeapYear: (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0,
    difference: Math.abs(
      Math.floor(
        (specifiedDate.getTime() - currentDate.getTime()) / (1000 * 3600 * 24)
      )
    ),
  };
};

module.exports = { square, reverse, date };
