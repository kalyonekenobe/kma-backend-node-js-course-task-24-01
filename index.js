require("dotenv").config();
const express = require("express");
const routes = require("./routes/app.routes");

const app = express();
const port = process.env.PORT || 56201;

app.use(routes);

app.listen(port, () => {
  console.log(`Application is running on port ${port}`);
});
