const express = require("express");
const controller = require("../controllers/app.controller");
const router = express.Router();

router.post("/square", express.text(), controller.square);
router.post("/reverse", express.text(), controller.reverse);
router.get("/date/:year/:month/:day", controller.date);

module.exports = router;
