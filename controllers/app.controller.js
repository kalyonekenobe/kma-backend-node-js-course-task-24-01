const appService = require("../services/app.service");
const valibot = require("valibot");

const square = async (req, res) => {
  try {
    const number = valibot.parse(valibot.number(), Number(req.body));
    const result = await appService.square(number);
    return res.status(201).json(result);
  } catch (error) {
    if (error instanceof valibot.ValiError) {
      return res.status(409).json({ error, status: 409 });
    }

    return res
      .status(500)
      .json({ error: "Internal server error", status: 500 });
  }
};

const reverse = async (req, res) => {
  try {
    const text = valibot.parse(valibot.string(), req.body);
    const result = await appService.reverse(text);
    return res.status(201).send(result);
  } catch (error) {
    if (error instanceof valibot.ValiError) {
      return res.status(409).send(error);
    }

    return res.status(500).send("Internal server error");
  }
};

const date = async (req, res) => {
  try {
    const year = valibot.parse(valibot.number(), Number(req.params.year));
    const month = valibot.parse(valibot.number(), Number(req.params.month));
    const day = valibot.parse(valibot.number(), Number(req.params.day));
    const result = await appService.date(year, month, day);
    return res.status(201).json(result);
  } catch (error) {
    if (error instanceof valibot.ValiError) {
      return res.status(409).json(error);
    }

    return res.status(500).json("Internal server error");
  }
};

module.exports = { square, reverse, date };
